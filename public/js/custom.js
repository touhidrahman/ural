$(document).ready(function(){

	var a = $('#pickup_addr');
	var b = $('#drop_addr');


	var pick_addr_html = '<select name="pickup_addr" id="pickup_addr" class="form-control input-xs">' +
                                                '<option value="">Select Airport</option>' +
                                                '<option value="Boston - Logan International Airport, Boston, MA, United States">Boston Logan</option>' +
                                                '<option value="JFK Terminal 1, NY, United States">JFK Airport</option>' +
                                            '</select>';
	var drop_addr_html = '<select name="drop_addr" id="drop_addr" class="form-control input-xs">' +
                                                '<option value="">Select Airport</option>' +
                                                '<option value="Boston - Logan International Airport, Boston, MA, United States">Boston Logan</option>' +
                                                '<option value="JFK Terminal 1, NY, United States">JFK Airport</option>' +
                                            '</select>';
	var type = $('#type');

	var drop_addr_field_old = $('#drop_addr_div').html();
	var pick_addr_field_old = $('#pick_addr_div').html();

	// change drop or pick addr field to dropdown based on value of type

	type.change(function(event) {

		if (type.val() == "Ride To Airport") {
			// change drop addr box
			$('#drop_addr_div').html(drop_addr_html);
		}

		if (type.val() != "Ride To Airport") {
			$('#drop_addr_div').html(drop_addr_field_old);
		}

		if (type.val() == "Ride From Airport") {
			// change pick addr box
			$('#pick_addr_div').html(pick_addr_html);
		}

		if (type.val() != "Ride From Airport") {
			$('#pick_addr_div').html(pick_addr_field_old);
		}

		// cannot be left blanck
		if (type.val() === '') {
			$('#complete_reservation_btn').addClass('disabled');
		} else {
			$('#complete_reservation_btn').removeClass('disabled');
		}
	});

	$('#vehicle').change(function(event) {
		if ($('#vehicle').val() === '') {
			$('#complete_reservation_btn').addClass('disabled');
		} else {
			$('#complete_reservation_btn').removeClass('disabled');
		}
	});

	// onchange events of few fields
	$('#stop_over').change(calcRoute);
	$('#infant_seat').change(calcRoute);
	$('#booster_seat').change(calcRoute);
	$('#regular_seat').change(calcRoute);
	$('#type').change(calcRoute);
	$('#vehicle').change(calcRoute);
	$('#lgg').change(calcRoute);
	$('#pax').change(calcRoute);


	// get distance from drop and pick address
	var directionsService = new google.maps.DirectionsService();
            
    function calcRoute() {
        var start = document.getElementById("pickup_addr").value;
        var end = document.getElementById("drop_addr").value;
        var distanceInput = document.getElementById("distance");
        //var distanceShow = document.getElementById("distance_show");
        
        var request = {
            origin:start, 
            destination:end,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                //directionsDisplay.setDirections(response);
                distanceInput.value = response.routes[0].legs[0].distance.value / 1609.34; // a Mile = 1609.34 m
                //distanceShow.value = response.routes[0].legs[0].distance.value / 1609.34; // a Mile = 1609.34 m
            }
        });

    }

    
    // end distance retrieve and set it to hidden field

	function price_display () {
		// in form estimation display logics
		var vehicle = $('#vehicle').val();
		var regular_seat = $('#regular_seat').val();
		var booster_seat = $('#booster_seat').val();
		var infant_seat = $('#infant_seat').val();
		var stop_over = $('#stop_over').val();

		var regular_seat_price = 5;
		var booster_seat_price = 5;
		var infant_seat_price = 5;
		var stop_over_price = 10;

		var distance = Math.round($('#distance').val());

		var per_mile = 0;

		if (vehicle == 'Sedan') { per_mile = 5; }
		if (vehicle == 'Van') { per_mile = 5.5; }
		if (vehicle == 'SUV') { per_mile = 6; }

		var svc_charge = 0;
		if ((type.val() == 'Ride To Airport') || (type.val() == 'Ride From Airport')) { svc_charge = 12; }

		var total = svc_charge + distance * per_mile + stop_over_price * stop_over + regular_seat_price * regular_seat + infant_seat_price * infant_seat + booster_seat_price * booster_seat;

		$('#t').html('$' + total);
		$('#tt').html(type.val());
		$('#tr').html('$' + svc_charge);
		$('#tv').html('$' + svc_charge);
		$('#pc').html($('#pax').val());
		$('#lc').html($('#lgg').val());
		$('#dc').html(distance);
		$('#dr').html('$' + per_mile);
		$('#dv').html('$' + per_mile * distance);
		$('#soc').html(stop_over);
		$('#sor').html('$' + stop_over_price);
		$('#sov').html('$' + stop_over_price * stop_over);
		$('#rsc').html(regular_seat);
		$('#rsr').html('$' + regular_seat_price);
		$('#rsv').html('$' + regular_seat_price * regular_seat);
		$('#isc').html(infant_seat);
		$('#isr').html('$' + infant_seat_price);
		$('#isv').html('$' + infant_seat_price * infant_seat);
		$('#bsc').html(booster_seat);
		$('#bsr').html('$' + booster_seat_price);
		$('#bsv').html('$' + booster_seat_price * booster_seat);

		// set total in the form's hidden field
		$('#total').val(total);
	}

	$('#complete_reservation_btn').click(function(){
		calcRoute(price_display());
		
	});


});
