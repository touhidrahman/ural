var mongoose = require('mongoose');

var reservationSchema = new mongoose.Schema({
  lastName: String,
  firstName: String,
  email: String,
  contact: String,
  service: String,
  vehicle: String,
  updated: Date,
  status: String,
  pickAddress: String,
  dropAddress: String,
  dt: Date,
  time: String,
  passenger: Number,
  luggage: Number,
  stopOver: Number,
  infantSeat: Number,
  boosterSeat: Number,
  regularSeat: Number,
  distance: Number,
  total: Number
});


var Reservation = mongoose.model('Reservation', reservationSchema);

module.exports = Reservation;