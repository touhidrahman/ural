var Reservation = require('../models/Reservation');

/**
 * GET /
 * Home page.
 */
exports.index = function(req, res) {
  res.render('home', {
    title: 'Home'
  });
};


exports.postReservation = function(req, res, next) {
	// validate form and save
	req.assert('lastName', 'Last name field cannot be empty').notEmpty();
  req.assert('firstName', 'First name field cannot be empty').notEmpty();
  req.assert('email', 'Use a valid email').notEmpty().isEmail();
  req.assert('contact', 'Phone number field cannot be empty').notEmpty();
  req.assert('vehicle', 'Please select vehicle type').notEmpty();
  //req.assert('pickAddress', 'Pick up address cannot be empty').notEmpty();
  //req.assert('dropAddress', 'Drop off address cannot be empty').notEmpty();
  req.assert('dt', 'Select the date you like to travel').notEmpty();
  req.assert('time', 'Select the time of your travel').notEmpty();
  req.assert('passenger', 'Select number of passengers').isInt();
  req.assert('luggage', 'Select number of luggages').isInt();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/');
  }

  var newReservation = Reservation({
    service     : req.body.type,
    vehicle     : req.body.vehicle,
    pickAddress : req.body.pickAddress,
    updated     : new Date(),
    status     : 'Pending',
    dropAddress : req.body.dropAddress,
    distance    : req.body.distance,
    dt          : req.body.dt,
    time        : req.body.time,
    passenger   : req.body.passenger,
    luggage     : req.body.luggage,
    stopOver    : req.body.stopOver,
    infantSeat  : req.body.infantSeat,
    boosterSeat : req.body.boosterSeat,
    regularSeat : req.body.regularSeat,
    firstName   : req.body.firstName,
    lastName    : req.body.lastName,
    contact     : req.body.contact,
    email       : req.body.email,
    total       : req.body.total
  });


  // save to db
  newReservation.save(function(err){
    if (err) throw err;
    // else 
    req.flash('success', { msg: 'Your reservation is complete!' });
    res.redirect('/reservation');
  });

  
}


exports.getReservation = function(req, res, next)  {
  // TODO updte view
  res.render('home', {
    title: 'Home'
  });
}